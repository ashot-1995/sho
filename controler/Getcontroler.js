const {
        User, 
        Product,
        ProductCategory, 
        ProductColor, 
        ProductSize, 
        ProductVariants,
        VariantsSize,
        ProductPicture, 
        Categories, 
        ProductCategories, 
        Cart, 
        Wishlist, 
        ProductStatus,
        Materials,
        ProductMaterials
    } = require("../model/user");
const {getShop, where} = require("sequelize");
const e = require("connect-flash");
const { load } = require("dotenv");
const jwt = require("jsonwebtoken")


class GetControler{
    constructor(){

    }

    async getSignUp(req,res){
        let object = {};
        if(req.session.error){
            for (let e of req.session.errors) {
                if(object[e.param]==undefined){
                    object[e.param]=e.msg
                }
            }
        }
        res.send({object})
    }



    async getLogin(req,res){
        // res.send({sign:req.session.error})
        let y = await User.findAll();
        res.send({user:y})
    }


    async getCategories(req, res) {
        let prodCat = await Categories.findAll()
        res.send({categories:prodCat})

    }

    async getMaterials(req, res) {
        let productMaterials = await Materials.findAll()
        res.send({materials:productMaterials})
    }


    async getProduct(req,res){
      let prod = await Product.findAll({attributes:[
            "id",
            "name",
            "slug",
            "short_desc",
            "price",
            "sale_price",
            "review",
            "code",
            "ratings",
            "until",
            "stock",
            "in_stock",
            "top",
            "featured",
            "new",
            "author",
            "sold",
        ]})
                
        for (let p of prod) {  
            let prodCat = await ProductCategories.findAll({where:{productId:p.dataValues.id},attributes:['categoryId'],raw : true })
            let prodMaterials = await ProductMaterials.findAll({where:{productId:p.dataValues.id},attributes:['materialId'],raw : true })
            let photo = await ProductPicture.findAll({where:{productId:p.dataValues.id},attributes:['url']})
            let variant = await ProductVariants.findAll({where:{productId:p.dataValues.id},attributes:['id','color','color_name','price']})
            let productSize = await ProductSize.findAll({where:{productId:p.dataValues.id},attributes:['name','slug']})
            let stat = await ProductStatus.findAll({where:{productId:p.dataValues.id},attributes:['title','value']})
            console.log(stat);

            for (let v of variant) {  
                let size = await VariantsSize.findAll({where:{variantId:v.dataValues.id},attributes:['id','name','slug']})
                v.dataValues.size = size
            }

            let cat = []
            for (let c of prodCat) {  
                cat.push(c.categoryId)
            
            }

            let mat = []
            for (let m of prodMaterials) {  
                mat.push(m.materialId)
            
            }

            let categories = await Categories.findAll({where:{id: cat},attributes:['id','name','slug']})
            let materials = await Materials.findAll({where:{id: mat},attributes:['id','name','slug']})
            p.dataValues.category = categories;
            p.dataValues.materials = materials;
            p.dataValues.pictures = photo;
            p.dataValues.variants = variant;
            p.dataValues.size = productSize;
            p.dataValues.status = stat;
          }
        res.send({products:prod})
    }





    async getCart(req,res){

        
        let cart = await Cart.findAll()

        let cartProdId = []
        for (let c of cart ) {
            cartProdId.push({prodId:c.dataValues.productId, qty:c.dataValues.qty})
        }


        for ( let prod of cartProdId ) {
            let currentProd = await Product.findAll({where:{id:prod.prodId
            }, attributes:[
                "id",
                "name",
                "slug",
                "short_desc",
                "code",
                "material",
                "price",
                "sale_price",
                "review",
                "ratings",
                "until",
                "stock",
                "in_stock",
                "top",
                "featured",
                "new",
                "author",
                "sold",
            ],raw : true })
            .then(data => data[0])

                let prodCat = await ProductCategories.findAll({where:{productId:currentProd.id},attributes:['categoryId'],raw : true })
                let variant = await ProductVariants.findAll({where:{productId:currentProd.id},attributes:['id','color','color_name','price']})
                for (let v of variant) {  
                    let size = await VariantsSize.findAll({where:{variantId:v.dataValues.id},attributes:['id','name','slug']})
                    v.dataValues.size = size
                }
    
                let cat = []
                for (let u of prodCat) {  
                    cat.push(u.categoryId)
                
                }
    
                let categories = await Categories.findAll({where:{id: cat},attributes:['name','slug']})
                currentProd.category = categories
                currentProd.variants = variant


                currentProd.qty = prod.qty

            res.send({data:currentProd})
            // res.send({data:cart})
        }
    }


    async getWishList(req, res) {
        let prodCat = await Wishlist.findAll()
        res.send({})

    }



    async adminprofile(req,res){
        req.flash('usId', req.user.id)
        res.send({status: "OK"})

    }

   

}

module.exports=new GetControler