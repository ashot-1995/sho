const bcrypt=require('bcrypt');
const {body,validationResult}=require("express-validator");
const {
        User,
        sequelize,
        Product,
        ProductColor,
        ProductSize, 
        ProductVariants, 
        VariantsSize, 
        ProductPicture, 
        Categories, 
        ProductCategories,  
        Cart, 
        Wishlist, 
        ProductStatus,
        Materials,
        ProductMaterials
    }=require("../model/user");
let passLength=10;

class PostControler{
    constructor(){

    }

    async signUp(req,res){
        let error=validationResult(req)
        let err=error.errors
        console.log(err)
        if(error.isEmpty()){
            let hash=bcrypt.hashSync(req.body.password,passLength)
            let x=await User.create({...req.body,password:hash});
            console.log(x);
            req.session.errors=undefined;
            req.flash('success_msg','you are registered');
        }
        else{
            req.session.errors=err
            req.flash('error_msg','you are not registered');
        }
        res.redirect('/register')
    }
   

    async addProduct(req, res){
        console.log('aaaaaaaaa', req.body)
        const reqFiles = [];
        const productCategoriesArr = [];
        const productMaterialsArr = [];
        const productPhotosArr = [];
        const productColorsArr = [];
        const productSizeArr = [];
        const url = req.protocol + '://' + req.get('host');
        for (let i = 0; i < req.files.length; i++) {
            reqFiles.push(url + '/photo/' + req.files[i].filename)
        }
        const parsedProduct = JSON.parse(req.body.product);
        const parsedProductCategories = JSON.parse(req.body.productCategories);
        const parsedProductMaterials = JSON.parse(req.body.productMaterials);
        const parsedProductVariants  = JSON.parse(req.body.productColors);
        const parsedProductSize = JSON.parse(req.body.productSize);
        const newProduct = await Product.create({...parsedProduct});
        for(let i = 0; i < parsedProductMaterials.length; i++) {
            productMaterialsArr.push({productId: newProduct.id, materialId: parsedProductMaterials[i]})
        }
        for(let i = 0; i < parsedProductCategories.length; i++) {
            productCategoriesArr.push({productId: newProduct.id, categoryId: parsedProductCategories[i]})
        }
        for(let i = 0; i < reqFiles.length; i++) {
            productPhotosArr.push({url: reqFiles[i], productId: newProduct.id})
        }
        for(let i = 0; i < parsedProductVariants.length; i++) {
            productColorsArr.push({color: parsedProductVariants[i].color, color_name:parsedProductVariants[i].en_name, productId: newProduct.id})
        }
        for(let i = 0; i < parsedProductSize.length; i++) {
            productSizeArr.push({name: parsedProductSize[i].name, slug: parsedProductSize[i].slug, productId: newProduct.id})
        }
        await ProductCategories.bulkCreate(productCategoriesArr);
        await ProductMaterials.bulkCreate(productMaterialsArr)
        await ProductPicture.bulkCreate(productPhotosArr);
        await ProductVariants.bulkCreate(productColorsArr);
        await ProductSize.bulkCreate(productSizeArr)
        res.redirect("/product")
    }

    async addProdStatus(req,res){
        await ProductStatus.create({...req.body}) 
        res.redirect("/product-status")
    }

    async addCategory(req,res) {
        await Categories.create({...req.body,catId:req.session.prod_id})
        res.redirect("/category")
    }

    async addMaterial(req,res) {
        await Materials.create({...req.body})
        res.redirect("/materials")
    }

    async deleteCategory(req, res) {
        const id = req.body.id;
        Categories.destroy({
            where: {
                id: id
            }
        });
        res.redirect("/category")
    }

    async deleteProduct(req, res) {
        const id = req.body.id;
        Product.destroy({
            where: {
                id: id
            }
        });
        res.redirect("/product")
    }

    async deleteMaterial(req, res) {
        const id = req.body.id;
        Materials.destroy({
            where: {
                id: id
            }
        });
        res.redirect("/materials")
    }

    async addProdColor(req,res){
        let color = await ProductColor.create({...req.body,colorId:req.session.prod_id})
        res.redirect("/product_colors")
    }

    async addProdSize(req,res){
        let color = await ProductSize.create({...req.body,sizId:req.session.prod_id})
        res.redirect("/product_size")
    }

    async addProdVariants(req,res){
        let variants = await ProductVariants.create({...req.body})
        res.redirect("/product_variants")
    }

    async addProdVariantsSize(req,res){
        let variants = await VariantsSize.create({...req.body,variantSizId:req.session.variantId})
        console.log(req.session);
        res.redirect("/variants_size")
    }

    async addToCart(req,res){
        let cart = await Cart.create({...req.body,cartId:req.session.productId})
        res.redirect("/to_cart")
    }

    async addWishList(req,res){
        let list = await Wishlist.create({...req.body,listId:req.session.productId})
        res.redirect("/wish_list")
    }

    async logout(rqe,res){
        delete req.session.us_id;
        res.redirect("/login")
    }



}

module.exports=new PostControler
