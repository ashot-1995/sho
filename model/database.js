const mysql=require('mysql')

class Database{
    constructor(){
        this.db=mysql.createConnection({
            host:'localhost',
            port:3306,
            user:'root',
            password:'',
            database:'shoesdb'
        })
    }
   

    
     findAll(table){
        return new Promise((resolve,reject)=>{
            this.db.query(`select * from ${table}`, (err,data)=>{
                if(err) throw err
                 if(data){
                    resolve(JSON.parse(data))
                }else{
                    resolve(data)
                }
            })
        })
    }
   



    findBy(table,data){
        let query=`select * from ${table} where `
        for(let e in data){
            query+=`${e}='${data[e]}' and `
        }
        query=query.substring(0,query.length-4)
        return new Promise((resolve,reject)=>{
            this.db.query(query,(err,data)=>{
                if(err) throw err;
                resolve(data)
            })
        })
    }


    // delete(table,data){
    //     let key=Object.keys(data);
    //     let val=Object.values(data)
    //     return new Promise((resolve,reject)=>{
    //         this.db.query(`DELETE FROM ${table} where ${key}='${val}'`),(err,data)=>{
    //             if(err) throw new err
    //             resolve(data)
    //         }
    //     })
    // }
    
   
}
module.exports=new Database