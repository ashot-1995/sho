const {Sequelize,Model,DataTypes, BOOLEAN}=require('sequelize')

const sequelize=new Sequelize('shoesdb','root','',{
    host:'localhost',
    dialect: 'mysql'
})




class User extends Model{}
User.init({
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        type:{
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        email:DataTypes.STRING,
        password:DataTypes.STRING


    },
    {
        modelName:'users',
        sequelize

    }
)
User.sync()



// add product
class Product extends Model{}
Product.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        name:DataTypes.STRING,
        slug:DataTypes.STRING,
        short_desc:DataTypes.STRING,
        code: DataTypes.STRING,
        price:DataTypes.DOUBLE,
        stock:DataTypes.INTEGER,
        in_stock:DataTypes.BOOLEAN,
        sale_price:DataTypes.DOUBLE,
        review:DataTypes.INTEGER,
        ratings:DataTypes.INTEGER,
        until:DataTypes.STRING,
        status:DataTypes.INTEGER,
        top:DataTypes.BOOLEAN,
        featured:DataTypes.STRING,
        new:DataTypes.BOOLEAN,
        author:DataTypes.BOOLEAN,
        sold:DataTypes.BOOLEAN,
    },
    {
        modelName:'product',
        sequelize
    }
)
Product.sync()

class ProductStatus extends Model{}
ProductStatus.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        title:DataTypes.STRING,
        value:DataTypes.STRING
    },
    {
        modelName:'status',
        sequelize
    }
)
ProductStatus.belongsTo(Product)
ProductStatus.sync()



class ProductPicture extends Model{}
ProductPicture.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        url:DataTypes.STRING,
    },
    {
        modelName:'pictures',
        sequelize
    }
)
ProductPicture.belongsTo(Product)
ProductPicture.sync()




class Categories extends Model{}
Categories.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        name:DataTypes.STRING,
        slug:DataTypes.STRING,
    },
    {
        modelName:'categories',
        sequelize
    }
)
Categories.sync()


class ProductCategories extends Model{}
ProductCategories.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
    },
    {
        modelName:'product_categorys',
        sequelize
    }
)
ProductCategories.belongsTo(Product, { onDelete: 'cascade' })
ProductCategories.belongsTo(Categories, { onDelete: 'cascade' })
ProductCategories.sync()


class Materials extends Model{}
Materials.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        name:DataTypes.STRING,
        slug:DataTypes.STRING,
    },
    {
        modelName:'materials',
        sequelize
    }
)
Materials.sync()

class ProductMaterials extends Model{}
ProductMaterials.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
    },
    {
        modelName:'product_materials',
        sequelize
    }
)
ProductMaterials.belongsTo(Product, { onDelete: 'cascade' })
ProductMaterials.belongsTo(Materials, { onDelete: 'cascade' })
ProductMaterials.sync()

class ProductColor extends Model{}
ProductColor.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        name:DataTypes.STRING,
        slug:DataTypes.STRING,
    },
    {
        modelName:'product_color',
        sequelize
    }
)
ProductColor.belongsTo(Product)
ProductColor.sync()



class ProductSize extends Model{}
ProductSize.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        name:DataTypes.STRING,
        slug:DataTypes.STRING,
    },
    {
        modelName:'product_size',
        sequelize
    }
)
ProductSize.belongsTo(Product)
ProductSize.sync()


class ProductVariants extends Model{}
ProductVariants.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        color:DataTypes.STRING,
        color_name:DataTypes.STRING,
        price:DataTypes.DOUBLE,
    },
    {
        modelName:'variants',
        sequelize
    }
)
ProductVariants.belongsTo(Product, { onDelete: 'cascade' })
ProductVariants.sync()


class VariantsSize extends Model{}
VariantsSize.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        name:DataTypes.STRING,
        slug:DataTypes.STRING,
    },
    {
        modelName:'variants_size',
        sequelize
    }
)
VariantsSize.belongsTo(ProductVariants)
VariantsSize.sync()



class Cart extends Model{}
Cart.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        qty:DataTypes.INTEGER,
    },
    {
        modelName:'cart',
        sequelize
    }
)
Cart.belongsTo(Product, { onDelete: 'cascade' }),
Cart.belongsTo(User)
Cart.sync()



class Wishlist extends Model{}
Wishlist.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
    },
    {
        modelName:'wish_list',
        sequelize
    }
)
Wishlist.belongsTo(Product, { onDelete: 'cascade' })
Wishlist.belongsTo(User, { onDelete: 'cascade' })
Wishlist.sync()




module.exports={

    User,
    sequelize,
    Product,
    ProductStatus,
    ProductPicture,
    Categories,
    ProductCategories,
    ProductColor,
    ProductSize,
    ProductVariants,
    VariantsSize,
    Cart,
    Wishlist,
    Materials,
    ProductMaterials
}
