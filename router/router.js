const express = require("express");
const getControler = require("../controler/Getcontroler");
const router=express.Router();
const {body}=require("express-validator");
const {User}=require('./../model/user');
const passport=require('passport')
const LocalStrategy=require('passport-local').Strategy
const bcrypt=require('bcrypt');
const Postcontroler = require("../controler/Postcontroler");
const multer = require('multer');
const db = require("../model/database");

const DIR = './public/photo/';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, Date.now() + '-' + fileName)
    }
});

const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
});


// type login
const isLoginedUser=(req,res, next)=>{
    if(req.isAuthenticated() && req.user.type==0){
        return next()
    }
    res.redirect("/")
}

const isLoginedAdmin=(req,res,next)=>{
    if(req.isAuthenticated() && req.user.type==1){
        return next()
    }
    res.redirect("/")
}


// get inquiry

// router.get("/signup", getControler.getSignUp);

router.get("/login", getControler.getLogin);

router.get("/profile", isLoginedUser);

router.get("/admin", isLoginedAdmin, getControler.adminprofile);

router.get("/product", getControler.getProduct);

router.get("/category", getControler.getCategories)

router.get("/data_cart", getControler.getCart)

router.get("/wish_list", getControler.getWishList)

router.get("/materials", getControler.getMaterials)


// post inquiry

router.post('/usRegistor',[
    body('email').notEmpty().withMessage('enter your email').isEmail().withMessage('enter email'),
    body('password').notEmpty().withMessage('enter your password').isLength({min:5,max:10}).withMessage('password length error')
],Postcontroler.signUp)


router.post('/useLogin',
        passport.authenticate('local', {
            failureFlash:true }),
            (req,res)=>{
                if(req.user){
                    req.session.us_id=req.user.id
                    if(req.user.type==1){
                        res.send('/admin')
                    } else{
                        res.send('/profile')
                    }
                } else {
                    res.send('/')
                }


            }
)





router.post("/addProd", upload.array("imgCollection"), Postcontroler.addProduct);

router.post("/addStatus", Postcontroler.addProdStatus)

// router.post("/addProductPhotos", upload.array("imgCollection"), Postcontroler.addProductPhotos);

router.post("/addCategory", Postcontroler.addCategory);

router.post("/addMaterial", Postcontroler.addMaterial);

// router.post("/addProCategory", Postcontroler.addProdCategory);

router.post("/addColor", Postcontroler.addProdColor);

router.post("/addSize", Postcontroler.addProdSize);

router.post("/addVariants", Postcontroler.addProdVariants);

router.post("/addVariantSize", Postcontroler.addProdVariantsSize)

router.post("/addCart", Postcontroler.addToCart)

router.post("/addTolist", Postcontroler.addWishList)

router.post("/logout", Postcontroler.logout);

router.post("/deleteCategory", Postcontroler.deleteCategory)

router.post("/deleteProduct", Postcontroler.deleteProduct)

router.post("/deleteMaterial", Postcontroler.deleteMaterial)




passport.use('local', new LocalStrategy(
    async function(username, password, done) {
       console.log("before")
      let user=await User.findOne({where:{email:username}}) 
        if (!user) {
         done(null, false, { message: 'Incorrect username.' });
        }
        if(!bcrypt.compareSync(password, user.password)){
            done(null, false, { message: 'Incorrect password.' });

        }
        done(null, user);
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});
  
passport.deserializeUser(async function(id, done) {
    console.log(id)
    done(null, await User.findOne({where:{id}}))
});


module.exports=router
